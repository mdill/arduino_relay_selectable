/*
20151213 - Created by Michael Dill

This sketch is inteded to work with a four-channel relay.

The user will open the serial monitor and select a number, 1-4, in order to
manipulate the relay.  By selecting a relay which is on, the sketch will turn it
off.  By selecting a relay which is off, the sketch will turn it on.

This sketch can be attached to an online interface, which will allow the user to
control electrical items when away from home.
*/

// Array containing the pins connected to the relay
int relay[] = { };
byte relay_count = 4;

// Create relay status arrays as off
int relay_state[] = { 1, 1, 1, 1 };

// Create relay pin input variable
byte pin;

// Create user choice and relay switch variables 
byte userChoice;
byte switchRelay;

// Create strings for user prompts
String turn = "Turning relay #";
String off = " off.\n";
String on = " on.\n";

void setup(){
    Serial.begin(115200);  // Begin serial monitor for user control

    // Retrieve the pins each relay is connected to
    for (byte y = 0; y < relay_count; y++){
        Serial.print("Which pin is relay #");
        Serial.print(y + 1);
        Serial.print(" connected to: ");

        while(Serial.available() == 0){
            pin = Serial.parseInt();
            assign(pin, y);
        }
    }

    for (byte x = 0; x < relay_count; x++){  // Set each relay pin as an
                                             // output and turn it off
        pinMode(relay[x], OUTPUT);
        digitalWrite(relay[x], relay_state[x]);
    }

    Serial.print("Please select a relay (1-4): ");  // Prompt user for
                                                    // initial input
}

void loop(){
    if (Serial.available() != 0){  // Wait for user's choice
        userChoice = Serial.parseInt();
        switchRelay = userChoice -1;  // Adjust choice for array

        // If user's choice isn't a physical relay
        if (userChoice != 1 && userChoice != 2 && userChoice != 3 &&
            userChoice != 4){
            Serial.println("\nYour choice is bad and you should feel bad.");
            Serial.println("");
            Serial.print("Please select a relay (1-4): ");
        }

        // If user's choice is an appropriate one
        else{
            Serial.println(userChoice);
            changerelay(userChoice, switchRelay);  // Pass user's choice to
                                                   // change function
        }
    }
}

void changerelay(byte choice, byte swap){
    if (relay_state[swap] == 0){  // If selected relay is off
        relay_state[swap] = 1;  // Turn selected relay on
        digitalWrite(relay[swap], relay_state[swap]);

        String output = turn + choice + off;  // Assemble output string
        Serial.print(output);  // Tell user what is happening
        Serial.println("");
    }

    else if (relay_state[swap] == 1){  // If selected relay is on
        relay_state[swap] = 0; // Turn selected relay off
        digitalWrite(relay[swap], relay_state[swap]);

        String output = turn + choice + on;  // Assemble output string
        Serial.print(output);  // Tell user what is happening
        Serial.println("");
    }

    Serial.print("Please select a relay (1-4): ");  // Prompt user for new
                                                    // relay
}

void assign(byte pins, byte num){
    // If pin is an actual pin on the Arduino
    if (pins <= 1 && pins >= 14){
        relay[num] = pins;
        Serial.println("");
    }

    // If the pin doesn't exist on an Arduino
    else{
        Serial.println("Don't you DARE lie to me!");
        Serial.println("");
        Serial.print("Let's try this again: ");
    }
 
}

