# Arduino Selectable Relay

## Purpose

This simple sketch is designed to test a 4-channel relay by waiting for a user
input integer between 1 and 4.  A selected relay module will be turned off if it
is on, and on if it is off.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/arduino_relay_selectable.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/arduino_relay_selectable/src/4c569043f1691ad14b2845fe1916a3e3a217d7bf/LICENSE.txt?at=master) file for
details.

